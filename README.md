# Common utility scripts

One of today's (20/06/2019) meeting conclusions was to make code more accessible

### pros:
- avoid useless work
- make source of inspiration for new lab members more accessible
- review/discuss code


### cons:
- This can become a mess if several people contribute, try to keep it tidy.

Since the scripts will be diverse, it would be great to keep the code comented & document functions thoroughly ~~rather than flooding the readme~~.  
Remember this repo is publicly available.  
If you have doubts about git, [here](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud) you can read Atlassian git tutorial.  