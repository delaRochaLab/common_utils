#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 14:49:52 2020

@author: molano
"""
import os
import sys
import numpy as np
import pandas as pd
import glob
from ast import literal_eval
import warnings
import matplotlib.pyplot as plt
import meta_info as mi
inf = '+INFO'
bft = 'BPOD-FINAL-TIME'
bit = 'BPOD-INITIAL-TIME'
model_cols = ['S1',
              'aftereff1', 'aftereff2', 'aftereff3', 'aftereff4',
              'aftereff5', 'aftereff6', 'aftereff7', 'aftereff8',
              'aftereff9', 'aftereff10',
              'L+1', 'L-1', 'L+2', 'L-2', 'L+3', 'L-3', 'L+4', 'L-4',
              'L+5', 'L-5', 'L+6-10', 'L-6-10',
              'T++1', 'T+-1', 'T-+1', 'T--1', 'T++2', 'T+-2', 'T-+2',
              'T--2', 'T++3', 'T+-3', 'T-+3', 'T--3', 'T++4', 'T+-4',
              'T-+4', 'T--4', 'T++5', 'T+-5', 'T-+5', 'T--5',
              'T++6-10', 'T+-6-10', 'T-+6-10', 'T--6-10', 'intercept']
afterc_cols = [x for x in model_cols if x not in ['L+2', 'L-1', 'T-+1',
                                                  'T+-1', 'T--1']]
aftere_cols = [x for x in model_cols if x not in ['L+1', 'T++1',
                                                  'T-+1', 'T+-1',
                                                  'T--1']]

warnings.filterwarnings('ignore')


def extr_listened_frames(soundvec, frames_listened):
    '''using this you take the risk of assuming that frames
    weight= throughout its duration in a future, || yep this is obsolete,
    use np.trapz(soundwave) instead
    soundvec: np.array, containing 20 amplitude values (stairs or frames)
    frames_listened: float, number of frames listened'''
    try:
        if frames_listened >= 20:  # with 20 it crashes
            # some time ago there were issues without .copy because simply
            # as a reference, original soundvec was modified.
            a = soundvec[:1+int(frames_listened)].copy()
            normvec = np.array([1]*20)
        else:
            a = soundvec[:1+int(frames_listened)].copy()
            normvec = np.concatenate((np.ones(int(frames_listened)),
                                      np.array([frames_listened % 1])))
        a = a*normvec
        return a.sum()/frames_listened
    except:
        return np.nan


def raw_to_trials(df1):
    """sorts bpod output to resorted tabular data (1 trial per row),
    this function can be used to iterate over a list of csv paths and
    stack all the data
    df1: data, can be either a pandas dataframe or the csv path"""

    if isinstance(df1, pd.DataFrame):
        pass  # good to go already
    elif isinstance(df1, str):
        df1 = pd.read_csv(df1, sep=';', skiprows=6, error_bad_lines=False)
    else:
        raise ValueError('arg shoud be a dataframe or an' +
                         ' (absolute or relative) path to the csv')
    # create trial-index column
    df1['trial_index'] = np.nan  # create emptycol to group rows by trial
    # set trial index to known rows (1-based)
    tmp = df1.MSG == 'coherence01'
    assert (tmp == (df1['MSG'] == 'coherence01')).all()
    df1.loc[(df1.TYPE == 'VAL') & (tmp), 'trial_index'] =\
        np.arange(1, 1+len(df1.loc[(df1.TYPE == 'VAL') & (tmp)]))
    df1['trial_index'].fillna(method='ffill', inplace=True)  # propagate indexes
    tmp = df1.TYPE == 'STATE'
    states = df1[tmp].sort_values(['trial_index',
                                   'BPOD-INITIAL-TIME']).reset_index(drop=True)
    states[inf] = states[inf].astype(float)
    fix = states[states.MSG == 'Fixation']
    # originally mixed dtype in the csv
    fix.loc[fix.MSG == 'Fixation', inf] =\
        fix.loc[fix.MSG == 'Fixation', inf].astype(float)
    # list of fb (in secs) before the actual complete fixation in that trial
    fb = fix.groupby(['trial_index', 'MSG'])[inf].\
        apply(list).apply(lambda x: np.array(x[:-1])).values
    # getting last registered rewside .values[-1](just in case it gets updated)
    tmp = states.MSG == 'Reward'
    hit = np.where(states[tmp][bft].astype(float) > 0, 1, np.nan)
    # fill remaining punishes
    tmp = states.MSG == 'Punish'
    hit[np.where(states[tmp][bft].astype(float) > 0)[0]] = 0
    # fill remaining invalids
    tmp = states.MSG == 'Invalid'
    hit[np.where(states[tmp][bft].astype(float) > 0)[0]] = -1
    num_trials = len(hit)
    # rawcoherences used in the task (from 0L to 1R)
    coh = df1[df1.MSG == 'coherence01'][inf].values[:num_trials]
    trialidx = np.arange(1, len(hit)+1, step=1)  # 1-based indexing
    tmp = df1.MSG == 'REWARD_SIDE'
    # dismiss last incomplete
    # WARNING this is not fully consistent with Albert's csv2mat
    rewside = np.array(literal_eval(df1.loc[df1.loc[tmp, inf].index.values[-1],
                                            inf]))[:num_trials]

    # get soundR failures
    sr_play = df1.loc[(df1.MSG.str.startswith('SoundR: Play.')) &
                      (df1.TYPE == 'stdout'), 'trial_index'].values[:num_trials]
    sr_stop = df1.loc[(df1.MSG.str.startswith('SoundR: Stop.')) &
                      (df1.TYPE == 'stdout'), 'trial_index'].values[:num_trials]
    soundrfail = sr_stop[np.isin(sr_stop, sr_play,  # 1 based trial index
                                 assume_unique=True, invert=True)].astype(int)

    # infer which was the response
    RResponse = np.zeros(num_trials)
    # right and correct
    RResponse[np.where(np.logical_and(rewside == 1, hit == 1))[0]] = 1
    # left and incorrect
    RResponse[np.where(np.logical_and(rewside == 0, hit == 0))[0]] = 1
    RResponse[~(hit >= 0)] = np.nan  # else, invalids would be L_responses

    # get envelopes
    lenv = df1[df1.MSG == 'left_envelope'][inf][:num_trials]
    renv = df1[df1.MSG == 'right_envelope'][inf][:num_trials]
    lenv = lenv.apply(lambda x: np.array(literal_eval(x)))  # strings to arrays
    renv = renv.apply(lambda x: np.array(literal_eval(x)))
    # we place those vectors/lists in our resulting dataframe (kek)
    kek = pd.DataFrame(data=np.array([trialidx, coh,  rewside,
                                      hit, RResponse]).T,
                       columns=['origidx', 'coh', 'rewside',
                                'hit', 'R_response'])
    # repetitions
    kek['rep_response'] =\
        ~(kek.R_response-kek.R_response.shift(1)).abs().values.astype(bool)
    kek['aftererror'] = ~kek.hit.shift(1).values.astype(bool)
    # we keep idx as int
    # FUTURE: to save memory we could change coh to indexes as in the old pipe
    for i in list(kek.columns)[1:]:
        kek[i] = kek[i].astype(float)

    subj = literal_eval(df1.loc[(df1.TYPE == 'INFO') &
                                (df1.MSG == 'SUBJECT-NAME'), inf].values[0])[0]
    sessid = df1[(df1.TYPE == 'INFO') &
                 (df1.MSG == 'SESSION-NAME')][inf].values[0]

    # add some id to be able to filter among sessions and animals
    kek['subjid'] = subj
    kek['sessid'] = sessid
    tmp = df1.MSG == 'TRIAL-BPOD-TIME'
    trialonset = df1[(df1.TYPE == 'INFO') &
                     (tmp)][bit].astype(float).values[:num_trials]
    tmp = df1.MSG == 'StartSound'
    soundonset = df1[(df1.TYPE == 'STATE') &
                     (tmp)][bit].astype(float).values[:num_trials]
    tmp = df1.MSG == 'WaitResponse'
    kek['resp_len'] =\
        df1.loc[(df1.TYPE == 'STATE') &
                (tmp), inf].values[:num_trials].astype(float)

    # paste previous vectors
    kek['lenv'] = lenv.values
    kek['renv'] = renv.values
    kek['res_sound'] = kek['lenv']+kek['renv']
    kek['trialonset'] = trialonset
    kek['soundonset'] = soundonset
    # buggy or faulty bpod?
    # old sessions have small fraction of out-of-states sound >1s
    sound_len = df1[(df1.MSG == 'StartSound') &
                    (df1.TYPE == 'STATE')][inf].astype(float).values[:num_trials]
    kek['sound_len'] = sound_len  # buggy ?
    kek['sound_len'] = kek['sound_len'].astype(float)*1000  # to ms
    kek['frames_listened'] = kek['sound_len']/50

    kek['av_listened_stim'] =\
        kek.apply(lambda x: extr_listened_frames(x['res_sound'],
                                                 x['frames_listened']), axis=1)

    kek['coh2'] = kek['coh']*2-1

    if len(df1[df1.MSG == 'prob_repeat'][inf].astype(float)) > 0:
        kek['prob_repeat'] =\
            df1[df1.MSG == 'prob_repeat'][inf].astype(float).values[:num_trials]
    else:
        kek['prob_repeat'] = np.nan  # dismiss old or find your own way

    # add withinblock index
    blen = int(df1.loc[(df1.TYPE == 'VAL') &
                       (df1.MSG == 'VAR_BLEN'), inf].values[-1])
    bnum = int(df1.loc[(df1.TYPE == 'VAL') &
                       (df1.MSG == 'VAR_BNUM'), inf].values[-1])
    kek['wibl_idx'] = np.tile(np.arange(1, blen+1, step=1), bnum)[:len(trialidx)]
    # block index
    kek['bl_idx'] = np.repeat(np.arange(1,  1+bnum), blen)[:len(trialidx)]
    kek['fb'] = fb
    kek['soundrfail'] = False
    kek.loc[kek.origidx.isin(soundrfail), 'soundrfail'] = True

    kek = kek[kek.frames_listened.notna()]  # drop last incomplete trial
    # get post-op flag
    post_op = df1[df1.MSG == 'VAR_POSTOP'][inf].values[0]
    # remove obvious stuff because this is shared XXX: check
    # kek.drop(['coh', 'res_sound', 'frames_listened'], inplace=True, axis=1)

    return kek, post_op, num_trials


def get_GLM_regressors(in_data):
    """
    input df object, since it will calculate history*, it must contain
    consecutive trials returns preprocessed dataframe.
    """
    df = in_data
    df.loc[:, 'rep_response'] *= 1  # originally is True/False col
    # expand stim [Sensory module]
    stim_it = range(1, 0, -1)
    df = pd.concat([df, pd.DataFrame(df["res_sound"].values.tolist(),
                    columns=["S" + str(x + 1) for x in range(20)],
                    index=df.index).loc[:, "S1":"S8"]], axis=1)

    # aftereffect regressors
    envmatrix = pd.DataFrame(df['res_sound'].values.tolist()).values
    nanmat = np.tile(np.array([1, np.nan]), df.shape[0]).reshape(-1, 2)
    # beware we have fucked up trials and i've lost 1 hour with this
    tvec = df.frames_listened.values
    tvec[tvec > 20] = 20
    wholeframes = tvec.astype(int)
    partialframes = np.zeros(tvec.size)
    partialframes[(tvec % 1) > 0] = 1
    partialframes[tvec == 20] = 0
    frames_listened_vec = (wholeframes + partialframes).astype(int)
    frames_mask = np.zeros((df.shape[0], 2))
    frames_mask[:, 0], frames_mask[:, 1] =\
        frames_listened_vec, 20-frames_listened_vec
    fuk = frames_mask.flatten().astype(int)
    frame_mat_mask = np.repeat(nanmat.flatten(), fuk).reshape(-1, 20)
    df['aftereff1'] = np.nansum((frame_mat_mask * envmatrix), axis=1)
    df['aftereff1'] = df.aftereff1.shift(1)
    df.loc[df.origidx == 1, 'aftereff1'] = np.nan
    for i in range(2, 11):
        df['aftereff'+str(i)] = df['aftereff'+str(i-1)].shift(1)
        df.loc[df.origidx == 1, 'aftereff'+str(i)] = np.nan

    for obsoletevariable in [envmatrix, nanmat, tvec, wholeframes,
                             partialframes, frames_listened_vec,
                             frames_mask, frame_mat_mask]:
        del obsoletevariable
    # Lateral module
    df['L+1'] = np.nan  # np.nan considering invalids as errors
    df.loc[(df.R_response == 1) & (df.hit == 1), 'L+1'] = 1
    df.loc[(df.R_response == 0) & (df.hit == 1), 'L+1'] = -1
    df.loc[df.hit == 0, 'L+1'] = 0
    df['L+1'] = df['L+1'].shift(1)
    df.loc[df.origidx == 1, 'L+1'] = np.nan
    # L-
    df['L-1'] = np.nan
    df.loc[(df.R_response == 1) & (df.hit == 0), 'L-1'] = 1
    df.loc[(df.R_response == 0) & (df.hit == 0), 'L-1'] = -1
    df.loc[df.hit == 1, 'L-1'] = 0
    df['L-1'] = df['L-1'].shift(1)
    df.loc[df.origidx == 1, 'L-1'] = np.nan
    # shifts
    for i, item in enumerate([2, 3, 4, 5, 6, 7, 8, 9, 10]):
        df['L+'+str(item)] = df['L+'+str(item-1)].shift(1)
        df['L-'+str(item)] = df['L-'+str(item-1)].shift(1)
        df.loc[df.origidx == 1, 'L+'+str(item)] = np.nan
        df.loc[df.origidx == 1, 'L-'+str(item)] = np.nan

    # add from 6 to 10, assign them and drop prev cols cols
    cols_lp = ['L+'+str(x) for x in range(6, 11)]
    cols_ln = ['L-'+str(x) for x in range(6, 11)]

    df['L+6-10'] = np.nansum(df[cols_lp].values, axis=1)
    df['L-6-10'] = np.nansum(df[cols_ln].values, axis=1)
    df.drop(cols_lp+cols_ln, axis=1, inplace=True)
    df.loc[df.origidx <= 6, 'L+6-10'] = np.nan
    df.loc[df.origidx <= 6, 'L-6-10'] = np.nan

    # pre transition module
    df.loc[df.origidx == 1, 'rep_response'] = np.nan
    df['rep_response_11'] = df.rep_response
    df.loc[df.rep_response == 0, 'rep_response_11'] = -1
    df.rep_response_11.fillna(value=0, inplace=True)
    df.loc[df.origidx == 1, 'aftererror'] = np.nan

    # transition module
    df['T++1'] = np.nan  # np.nan
    df.loc[(df.aftererror == 0) & (df.hit == 1), 'T++1'] =\
        df.loc[(df.aftererror == 0) & (df.hit == 1), 'rep_response_11']
    df.loc[(df.aftererror == 1) | (df.hit == 0), 'T++1'] = 0
    df['T++1'] = df['T++1'].shift(1)

    df['T+-1'] = np.nan  # np.nan
    df.loc[(df.aftererror == 0) & (df.hit == 0), 'T+-1'] =\
        df.loc[(df.aftererror == 0) & (df.hit == 0), 'rep_response_11']
    df.loc[(df.aftererror == 1) | (df.hit == 1), 'T+-1'] = 0
    df['T+-1'] = df['T+-1'].shift(1)

    df['T-+1'] = np.nan  # np.nan
    df.loc[(df.aftererror == 1) & (df.hit == 1), 'T-+1'] =\
        df.loc[(df.aftererror == 1) & (df.hit == 1), 'rep_response_11']
    df.loc[(df.aftererror == 0) | (df.hit == 0), 'T-+1'] = 0
    df['T-+1'] = df['T-+1'].shift(1)

    df['T--1'] = np.nan  # np.nan
    df.loc[(df.aftererror == 1) & (df.hit == 0), 'T--1'] =\
        df.loc[(df.aftererror == 1) & (df.hit == 0), 'rep_response_11']
    df.loc[(df.aftererror == 0) | (df.hit == 1), 'T--1'] = 0
    df['T--1'] = df['T--1'].shift(1)

    # shifts now
    for i, item in enumerate([2, 3, 4, 5, 6, 7, 8, 9, 10]):
        df['T++'+str(item)] = df['T++'+str(item-1)].shift(1)
        df['T+-'+str(item)] = df['T+-'+str(item-1)].shift(1)
        df['T-+'+str(item)] = df['T-+'+str(item-1)].shift(1)
        df['T--'+str(item)] = df['T--'+str(item-1)].shift(1)
        df.loc[df.origidx == 1, 'T++'+str(item)] = np.nan
        df.loc[df.origidx == 1, 'T+-'+str(item)] = np.nan
        df.loc[df.origidx == 1, 'T-+'+str(item)] = np.nan
        df.loc[df.origidx == 1, 'T--'+str(item)] = np.nan

    cols_tpp = ['T++'+str(x) for x in range(6, 11)]
    # cols_tpp = [x for x in df.columns if x.startswith('T++')]
    cols_tpn = ['T+-'+str(x) for x in range(6, 11)]
    # cols_tpn = [x for x in df.columns if x.startswith('T+-')]
    cols_tnp = ['T-+'+str(x) for x in range(6, 11)]
    # cols_tnp = [x for x in df.columns if x.startswith('T-+')]
    cols_tnn = ['T--'+str(x) for x in range(6, 11)]
    # cols_tnn = [x for x in df.columns if x.startswith('T--')]

    df['T++6-10'] = np.nansum(df[cols_tpp].values, axis=1)
    df['T+-6-10'] = np.nansum(df[cols_tpn].values, axis=1)
    df['T-+6-10'] = np.nansum(df[cols_tnp].values, axis=1)
    df['T--6-10'] = np.nansum(df[cols_tnn].values, axis=1)
    df.drop(cols_tpp+cols_tpn+cols_tnp+cols_tnn, axis=1, inplace=True)
    df.loc[df.origidx < 6, ['T++6-10', 'T+-6-10', 'T-+6-10', 'T--6-10']] =\
        np.nan
    # transforming transitions to left/right space
    for col in [x for x in df.columns if x.startswith('T')]:
        df[col] = df[col] * (df.R_response.shift(1)*2-1)
        # {0 = Left; 1 = Right, nan=invalid}
    for i in stim_it:
        df.loc[df.frames_listened < (i-1), 'S'+str(i)] = 0

    df['intercept'] = 1
    df.loc[:, model_cols].fillna(value=0, inplace=True)

    if 'soundrfail' in df.columns.tolist():
        # just replace res_sound
        ent_series = df['res_sound']
        soundrfailidx = df.loc[df.soundrfail, 'res_sound'].index
        # df.loc[df.soundrfail==True, 'res_sound'] =
        # np.zeros(df.soundrfail.sum(),20) #* df.soundrfail.sum()
        ent_series[soundrfailidx] = [np.zeros(20)]*soundrfailidx.size
        df['res_sound'] = ent_series
    return df  # resulting df with lateralized T+


def load_data_exp(path, tag='p4_u_', bef_aft_les='all'):
    name = os.path.basename(path)
    assert os.path.exists(path+'/sessions'), path+'/sessions does not exist'
    folders = glob.glob(path+'/sessions/*'+tag+'*')
    folders = order_by_sufix(folders)
    num_tr_sess = []
    files_used_txt = ''
    for ind_f, f in enumerate(folders):
        if check_date(exp=name, date=f[0], bef_aft_les=bef_aft_les):
            file = glob.glob(f[1]+'/*.csv')
            if len(file) == 1:
                # data, post_op, ntr = GetParameters(path=file[0], data=data)
                # data, post_op, ntr = read_session(targ=file[0])
                df, post_op, ntr = raw_to_trials(file[0])
                df = get_GLM_regressors(df)
                try:
                    df_master = pd.concat([df_master, df])
                except NameError as e:
                    print(e)
                    df_master = df
                assert (post_op == 'none' or bef_aft_les == 'after' or
                        bef_aft_les == 'all')
                num_tr_sess.append(ntr)
                files_used_txt += file[0] + '\n'
            elif len(file) > 1:
                raise IOError('Too many files in folder')
    print('Preprocessing done.')
    return df_master, files_used_txt, num_tr_sess


def order_by_sufix(file_list):
    sfx = [int(x[x.rfind('_')+1:x.rfind('-')]) for x in file_list]
    sorted_list = [[y, x] for y, x in sorted(zip(sfx, file_list))]
    return sorted_list


def check_date(exp, date, bef_aft_les):
    if exp in mi.rats_info.keys():
        exp_data = mi.rats_info[exp]
        if date >= exp_data['discard'][0] and date <= exp_data['discard'][1]:
            return False
        elif bef_aft_les == 'before' and date >= exp_data['lesion_date']:
            return False
        elif bef_aft_les == 'after' and date <= exp_data['lesion_date']:
            return False
    return True



def to_minus_plus(a):
    """
    translate to -1 +1
    """
    return 2*(a-0.5)


def check_data_frame(df_master, start=0, num_tr=200):
    plt.figure()
    plt.plot(df_master['R_response'].values[start:start+num_tr], '-+',
             label='resp')
    plt.plot(df_master['rewside'].values[start:start+num_tr], '--+',
             label='rewside')
    plt.plot(df_master['T++1'].values[start:start+num_tr], '-+',
             label='T++1')
    for ind in range(num_tr):
        plt.plot([ind, ind], [-2, 1], '--k', lw=0.5)
    plt.legend()


if __name__ == '__main__':
    plt.close('all')
    path_to_rat_files = '/home/manuel/dms_lesions/psytrack_tests/le36'
    tag = '_p4_u_'
    # get dataframes with all variables/regressors concatenated for all sessions
    # the function load_data_exp allows filtering sessions with the
    # variable tag, e.g. tag = '_p4_u_' means "take only the sessions that have
    # in its name the string '_p4_u_'
    df_master, files_used_txt, num_tr_sess =\
        load_data_exp(path=path_to_rat_files, tag=tag)
    # the function below allows plotting the variables in df_master to check
    # that everything makes sense. Right now, the function plots the response,
    # the reward-side and the T++1, but it is straightforward to plot any other
    # variable by modifying the function.
    check_data_frame(df_master, start=0, num_tr=50)
