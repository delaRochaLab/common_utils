#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 11:07:29 2020

@author: molano
"""

rats_info = {'le36': {'lesion_date': 20190608,
                      'discard': [20190508, 20190523]},
             'le37': {'lesion_date': 20190608,
                      'discard': [20190508, 20190523]},
             'le38': {'lesion_date': 20190819,
                      'discard': [20190508, 20190523]},
             'le39': {'lesion_date': 20190609,
                      'discard': [20190508, 20190523]},
             'le40': {'lesion_date': 20190609,
                      'discard': [20190408, 20190523]},
             'le41': {'lesion_date': 20190820,
                      'discard': [20190408, 20190523]}}
