# j
# this should convert bcontrol .mat files in inpath to 2 x pd.DataFrame.to_pickle in outpath (rows=trials and rows=states, works with cristina's early CoM sessions)
# requires some tweaking to work in other tasks (eg this task has 2 coh)
# beware filenames because they are supposed to contain useful info and follow some patterns 
# this was written to run in the cluster.

import sys # intended to specify number of logical cores when running this, never implemented
import numpy as np
import pandas as pd
import os
import scipy.io as spio
from multiprocessing import Pool
#from tqdm import tqdm
import datetime
import logging

logfile = './exception_tracebacks.txt'
logging.basicConfig(filename=logfile, level=logging.DEBUG)
start = datetime.datetime.now()
logging.debug(f'testing mat2pdpkl traceback logging on {start.isoformat()}')

print(f'log: mat2pdpkl.py run on ´{start.strftime("%Y-%m-%d %H:%M:%S")}', file=open("log.txt", "a"))

inpath = './input/' # where are .mat files to convert
outpath = './output/' # where to save pickled dataframes


#https://stackoverflow.com/questions/7008608/scipy-io-loadmat-nested-structures-i-e-dictionaries
def loadmat(filename):
	'''
	this function should be called instead of direct spio.loadmat
	as it cures the problem of not properly recovering python dictionaries
	from mat files. It calls the function check keys to cure all entries
	which are still mat-objects
	'''
	data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
	return _check_keys(data)

def _check_keys(dict):
	'''
	checks if entries in dictionary are mat-objects. If yes
	todict is called to change them to nested dictionaries
	'''
	for key in dict:
		if isinstance(dict[key], spio.matlab.mio5_params.mat_struct):
			dict[key] = _todict(dict[key])
	return dict        

def _todict(matobj):
	'''
	A recursive function which constructs from matobjects nested dictionaries
	'''
	dict = {}
	for strg in matobj._fieldnames:
		elem = matobj.__dict__[strg]
		if isinstance(elem, spio.matlab.mio5_params.mat_struct):
			dict[strg] = _todict(elem)
		else:
			dict[strg] = elem
	return dict

def gendic(cohvec):
    """used later to map indexes to values"""
    d = {}
    for i, item in enumerate(cohvec.tolist()):
        d[i+1]=item
    return d

def destroy_mat(target):
    '''main function to get rid of annoying structs
    target: filename (w/o path, which is inpath var)'''
    try:
        print(f'started {target} conversion...')
        lmao = loadmat(inpath+target)
        

        # hopefully this is consistent...
        a = target.split('@')[1]
        subj = a.split('_')[5] #beware
        protocolid = '_'.join(a.split('_')[:4])
        sessid = '_'.join(a.split('_')[5:])[:-4]


        # get stuff
        nTrial = lmao['saved']['ProtocolsSection_n_completed_trials']
        nValidTrial = lmao['saved'][protocolid+'_NvalidTrials']
        HitHistoryList = lmao['saved'][protocolid+'_HitHistory']
        ParsedEvents = lmao['saved_history']['ProtocolsSection_parsed_events']
        CoherenceVec = lmao['saved'][protocolid+'_CoherenceVec'] # (2,) in v1 | v3 = [-1,-0.5,0,0.5,1]
        RewardSideList = lmao['saved'][protocolid+'_RewardSideList'] # dict_keys(['labels', 'values'])
        StimulusList = lmao['saved'][protocolid+'_StimulusList']['values'] # dict_keys(['values']) ## single key dict
        CubeName = lmao['saved'][protocolid+'_CubeNumber'] # Seed1
        CoherenceList1 = lmao['saved'][protocolid+'_CoherenceList1']['values'] # dict with single key : 'values'
        CoherenceList2 = lmao['saved'][protocolid+'_CoherenceList2']['values']
        trialidxes = np.arange(1,nTrial,1)

        useful_states = ['wait_for_cpoke','pre_stim_delay','early_withdrawal_pre','play_target', 'wait_for_apoke','reward','classification_error_trial','timeout_trial']

        state_list=[]
        for trial in trialidxes.tolist():
            for state in useful_states:
                state_list += [getattr(ParsedEvents[trial].states, state)]


        stackedstates = np.empty(0)
        stackedstatesname = []
        trialidxlist = []
        for trial in trialidxes.tolist():
            for state in useful_states:
                currstate = getattr(ParsedEvents[trial].states, state)
                if currstate.size<1:
                    stackedstates = np.append(stackedstates,np.array([np.nan,np.nan]))
                    stackedstatesname += [state]
                    trialidxlist += [trial]
                elif currstate.size>2:
                    nrep = int(currstate.size/2)
                    stackedstatesname += [state]*nrep
                    trialidxlist += [trial]*nrep
                    stackedstates= np.append(stackedstates, currstate.ravel())
                else:
                    stackedstates = np.append(stackedstates, currstate)
                    stackedstatesname += [state]
                    trialidxlist += [trial]
        stackedstates = stackedstates.reshape(-1,2)
        b=pd.DataFrame(data={'trial':trialidxlist, 'state':stackedstatesname, 'stime':stackedstates[:,0].flatten(), 'etime':stackedstates[:,1].flatten()})
        b['len'] = b['etime']-b['stime']
        hithistory=np.where(b[b.state=='reward']['len']>0,1,np.nan) # hit
        hithistory[np.where(b[b.state=='classification_error_trial']['len']>0)[0]]=0 # miss
        hithistory[np.where(b[b.state=='timeout_trial']['len']>0)[0]]= -1 # inv
        rewside = RewardSideList['values'][trialidxes]
        coh1 = CoherenceList1[trialidxes]
        coh2 = CoherenceList2[trialidxes]

        RResponse = np.zeros(len(hithistory))
        RResponse[np.where(np.logical_and(rewside==1, hithistory==1)==True)[0]] = 1 # right and correct
        RResponse[np.where(np.logical_and(rewside==0, hithistory==0)==True)[0]] = 1 # left and incorrect 
        RResponse[hithistory==-1]=np.nan

        kek = pd.DataFrame(data=np.array([np.arange(1,len(hithistory)+1), coh1, coh2, rewside, hithistory, RResponse]).T, 
                   columns=['trial','coh1','coh2','rewside', 'hithistory','R_response'])
        kek['aftererror']= (~(kek['hithistory'].shift(1).astype(bool)))*1

        kek['coh1']=kek.coh1.map(gendic(CoherenceVec)).values
        kek['coh2']=kek.coh2.map(gendic(CoherenceVec)).values
        fix = b[b.state=='pre_stim_delay']
        fb = fix.groupby(['trial','state'])['len'].apply(list).apply(lambda x: np.array(x[:-1])).values
        kek['fb'] = fb
        kek['sessid'] = np.repeat(sessid, len(kek))

        b['sessid'] = np.repeat(sessid, len(b))

        b.to_pickle(outpath+target[:-4]+'_states.pkl')
        kek.to_pickle(outpath+target[:-4]+'_trials.pkl')
    except:
        logging.exception(f'exception while processing {target}')
        print(f'\tfailed processing: {target}', file=open("log.txt", "a"))

targets = os.listdir(inpath)
targets = [x for x in targets if x.endswith('.mat')]


p = Pool()
p.map(destroy_mat, targets)
p.close()
p.join()

print(f'finished at: {datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}')
